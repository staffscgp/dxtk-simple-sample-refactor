//--------------------------------------------------------------------------------------
// File: SimpleSample.cpp
//
// This is a simple Win32 desktop application showing use of DirectXTK
//
// http://go.microsoft.com/fwlink/?LinkId=248929
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#include "Window.h"
#include "Graphics.h"
#include "TextureManager.h"

using namespace DirectX;

Graphics * graphics = new Graphics();
TextureManager * textureManager = new TextureManager();

int textureID = 0;

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
void Render();

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
    UNREFERENCED_PARAMETER( hPrevInstance );
    UNREFERENCED_PARAMETER( lpCmdLine );

	Window * window = new Window();

    if (FAILED(window->InitWindow(hInstance, nCmdShow)))
        return 0;

	if (FAILED(graphics->InitDevice(window->getWindowHandle())))
        return 0;

	textureID = textureManager->LoadTexture(graphics->GetDevice(), L"windowslogo.dds");

    // Main message loop
    MSG msg = {0};

    while (WM_QUIT != msg.message)
    {
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            Render();
        }
    }

	delete graphics;
	graphics = nullptr;

	delete window;
	window = nullptr;

    return (int)msg.wParam;
}

//--------------------------------------------------------------------------------------
// Render a frame
//--------------------------------------------------------------------------------------
void Render()
{
	graphics->ClearRenderTarget();

    // Draw sprite
	graphics->BeginSpriteDrawing();
	graphics->Draw(&textureManager->GetTexture(textureID), XMFLOAT2(10, 75), Colors::White);

    graphics->DrawString(L"DirectXTK Simple Sample", XMFLOAT2(100, 10), Colors::Yellow);
    graphics->EndSpriteDrawing();

	graphics->Present();
}
